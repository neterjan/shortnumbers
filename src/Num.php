<?php

declare(strict_types=1);

namespace ShortNumbers;

use ShortNumbers\Convert\LongNumber;
use ShortNumbers\Convert\ShortNumber;

class Num {

	const SI = [
		'D' => 10 ** 1,
		//10
		//deka
		'K' => 10 ** 3,
		//1 000
		//kilo
		'M' => 10 ** 6,
		//1 000 000
		//mega
		'G' => 10 ** 9,
		//or
		'B' => 10 ** 9,
		//1 000 000 000
		//giga
		'T' => 10 ** 12,
		//tera
		'd' => 10 ** 1,
		'k' => 10 ** 3,
		'm' => 10 ** 6,
		'g' => 10 ** 9,
		't' => 10 ** 12,
	];

	/**
	 * @param int $input
	 *
	 * @return string
	 *
	 * @example 20500 => 20.5K
	 */
	public static function toShort(int $input): string {

		$num = new LongNumber($input);

		return $num->toShort();

	}

	/**
	 * @param string $input
	 *
	 * @return float
	 *
	 * @example 20.5K => 20500
	 */
	public static function toLong(string $input): float {

		$num = new ShortNumber($input);

		return $num->toLong();

	}

}
