<?php

declare(strict_types=1);

namespace ShortNumbers\Convert;

use ShortNumbers\Num;

class LongNumber
{
    /** @var string */
    protected $input;

    /** @var integer */
    protected $order;//řád čísla

    /**
     * LongNumber constructor.
     *
     * @param int $input
     */
    public function __construct(int $input)
    {
        $this->input = $input;
        $this->findOrder();
    }

    /**
     * @return $this
     */
    public function findOrder(): self
    {
        $keys = array_keys(Num::SI);
        $i = 0;
        $si = array_slice(Num::SI, 0, (int) (count(Num::SI) / 2));

        if ($this->input < $si['D']) {
            $this->order = 1;

            return $this;
        }

        foreach ($si as $k => $value) {
            if ($this->input >= $si[$keys[$i]] && $this->input < $si[$keys[$i + 1]]) {//next
                $this->order = $si[$k];

                return $this;
            }
            $i++;
        }
        $this->order = end($si);

        return $this;
    }

    /**
     * @param int $precision
     * @return string
     */
    public function toShort(int $precision = 2): string
    {
        if ($this->input < 10000) {
            return number_format($this->input, 0, '.', ' ');
        }

        return round($this->input / $this->order, $precision) . array_search($this->order, Num::SI);
    }
}
