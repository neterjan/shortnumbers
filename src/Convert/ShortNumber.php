<?php

declare(strict_types=1);

namespace ShortNumbers\Convert;

use ShortNumbers\Num;

class ShortNumber
{
    /** @var string */
    protected $input, $suffix, $separator;

    /** @var boolean */
    protected $space, $isValid;

    /** @var integer */
    protected $characteristic,//Before Comma
        $mantissa,//After Comma
        $multiply;

    const DEFAULT_SEPARATOR = '.';

    /**
     * ShortNumber constructor.
     *
     * @param string $input
     *
     * @example 10.5K, 20,5 M,
     */
    public function __construct(string $input)
    {
        $this->input = $input;
        $this->parseData();
    }

    public function parseData(): void
    {
        $multiples = implode('', array_keys(Num::SI));
        preg_match('/([0-9]{1,20})([,.]{0,1})\s{0,1}([0-9]{0,8})([' . $multiples . ']{0,1})/', $this->input, $matches);
        $this->characteristic = (int)$matches[1];
        $this->separator = (string)$matches[2];
        $this->mantissa = (int)$matches[3];
        $this->suffix = (string)$matches[4];
        if ($this->suffix) {
            $this->multiply = (int)Num::SI[$this->suffix];
        } else {
            $this->multiply = 1;
        }
    }

    /**
     * @return float|int
     */
    public function toLong()
    {
        return (float)($this->characteristic . '.' . $this->mantissa) * $this->multiply;
    }
}
