<?php

use ShortNumbers\Num;

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

$nums = [
    '250',
	'250K',
	'250 k',
	'250k',
	'250M'
];
foreach($nums as $num) {
	echo $num . ' => ' . Num::toLong($num) . "<br/>";
}